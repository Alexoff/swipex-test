import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './components/catalog/catalog.component';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {catalogReducer} from './components/actions/reducer/catalog.reducers';
import {CatalogEffects} from './components/actions/effects/catalog.effects';



@NgModule({
  declarations: [CatalogComponent],
  imports: [
    CommonModule,
    StoreModule.forRoot({catalog: catalogReducer}),
    EffectsModule.forRoot([CatalogEffects])
  ]
})
export class CatalogModule { }
