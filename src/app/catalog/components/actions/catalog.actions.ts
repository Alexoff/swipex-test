import {Action} from '@ngrx/store';
import {Catalog} from '../../models/catalog.model';
import {HttpParams} from '@angular/common/http';


export enum CatalogActionTypes {
  LoadCatalog = '[Catalog] Load catalog action',
  LoadCatalogSuccess = '[Catalog] Load catalog success action'
}

export class LoadCatalogAction implements Action {
  public readonly type = CatalogActionTypes.LoadCatalog;
  public query = undefined;

  constructor(public page?: string) {
    this.query = this.page ? {page} : {};
  }
}

export class LoadCatalogSuccessAction implements Action {
  public readonly type = CatalogActionTypes.LoadCatalogSuccess;

  constructor(public catalog: Catalog[], public totalPages: number, public currentPage: number) {
  }
}

export type CatalogAction =
  LoadCatalogAction |
  LoadCatalogSuccessAction;
