import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CatalogService} from '../../../services/catalog.service';
import {Store} from '@ngrx/store';
import {State} from '../reducer/catalog.reducers';
import {CatalogActionTypes, LoadCatalogAction, LoadCatalogSuccessAction} from '../catalog.actions';
import {map, switchMap} from 'rxjs/operators';
import {CatalogInfo} from '../../../models/catalog-info.model';


@Injectable()
export class CatalogEffects {

  constructor(private actions: Actions,
              private catalogService: CatalogService,
              public store: Store<State>) {
  }

  @Effect()
  loadCatalog = this.actions.pipe(
    ofType(CatalogActionTypes.LoadCatalog),
    switchMap((action: LoadCatalogAction) => {
      return this.catalogService.getCatalog(action.query).pipe(
        map((catalogInfo: CatalogInfo) => new LoadCatalogSuccessAction(catalogInfo.catalog, catalogInfo.totalPages, catalogInfo.currentPage))
      );
    })
  );

}
