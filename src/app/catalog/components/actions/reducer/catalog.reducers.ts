import {Catalog} from '../../../models/catalog.model';
import {CatalogAction, CatalogActionTypes} from '../catalog.actions';


export interface State {
  catalog: Catalog[];
  currentPage: number;
  totalPages: number;
}

const initialState: State = {
  catalog: [],
  currentPage: null,
  totalPages: null
};

export function catalogReducer(state = initialState, action: CatalogAction) {
  switch (action.type) {
    case CatalogActionTypes.LoadCatalogSuccess:
      return {
        ...state,
        catalog: action.catalog,
        totalPages: action.totalPages,
        currentPage: action.currentPage
      };
    default:
      return state;
  }
}
