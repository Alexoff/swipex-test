import { Component, OnInit } from '@angular/core';
import {Catalog} from '../../models/catalog.model';
import {Store} from '@ngrx/store';
import {State} from '../actions/reducer/catalog.reducers';
import {LoadCatalogAction} from '../actions/catalog.actions';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  public catalog: Catalog[];
  public totalPages: number;
  public currentPage: number;

  constructor(private store: Store<State>) {
    this.store.dispatch(new LoadCatalogAction());
    this.store.select('catalog').subscribe((state: any) => {
      this.catalog = state.catalog;
      this.totalPages = state.totalPages;
      this.currentPage = state.currentPage;
    });
  }

  ngOnInit(): void {
  }

  counter(num: number): Array<number> {
    return new Array(num * 1);
  }

  loadPage(page?: string) {
    this.store.dispatch(new LoadCatalogAction(page));
  }

}
