import {Catalog} from './catalog.model';

export class CatalogInfo {
  public catalog: Catalog[];
  public currentPage: number;
  public totalPages: number;

  constructor(obj: any) {
    this.totalPages = obj.total_pages;
    this.currentPage = obj.page;
    this.catalog = obj.data.map(person => new Catalog(person));
  }
}
