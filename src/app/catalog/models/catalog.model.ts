export class Catalog {
  id: number;
  avatar: string;
  email: string;
  firstName: string;
  lastName: string;

  constructor(obj: any) {
    this.id = obj.id;
    this.avatar = obj.avatar;
    this.email = obj.email;
    this.firstName = obj.first_name;
    this.lastName = obj.last_name;
  }
}
