import {Injectable, Query} from '@angular/core';
import {HttpWrapper} from '../../wrappers/http.wrapper';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Catalog} from '../models/catalog.model';
import {Observable} from 'rxjs';
import {CatalogInfo} from '../models/catalog-info.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  private baseApiUrr = environment.baseApiUrl;

  constructor(private httpClient: HttpClient) {
  }

  getCatalog(param?: HttpParams): Observable<CatalogInfo> {
    return this.httpClient.get(`${this.baseApiUrr}/users`, {params: param}).pipe(
      map( (catalogInfo: any) => new CatalogInfo(catalogInfo))
    );
  }
}
