import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class HttpWrapper {
  private baseApiUrl = environment.baseApiUrl;

  constructor(protected http: HttpClient) {
  }

  post(url: string, body: any, options?: any): Observable<any> {
    return this.http.post<any>(this.baseApiUrl + url, body, options);
  }

  put(url: string, body: any, options?: any): Observable<any> {
    return this.http.put<any>(this.baseApiUrl + url, body, options);
  }

  get(url: string, options?: any): Observable<any> {
    return this.http.get<any>(this.baseApiUrl + url, options);
  }

  delete(url: string, options?: any): Observable<any> {
    return this.http.delete<any>(this.baseApiUrl + url, options);
  }
}
